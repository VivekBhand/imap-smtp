from imap import *
from smtp import *
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import os
import threading
import getpass
# def menu():
# username = input("Enter Email ID here: ")
# password = getpass.getpass()
while True:
    os.system('clear')
    print(Fore.LIGHTBLUE_EX+ "\t\t\t EMAIL CLIENT"+Style.RESET_ALL)
    username = input("\n\nEnter Email ID here: ")
    password = getpass.getpass()
    print(Fore.LIGHTGREEN_EX + "\n\nPlease wait till connection is done"+Style.RESET_ALL)

    if 'gmail.com' in username:
            IMAP_SERVER =  'imap.gmail.com'		
            IMAP_PORT = 993
    elif '@coep.ac.in' in username:
        IMAP_SERVER = 'imap-mail.outlook.com'
        IMAP_PORT = 993
    IMAP_client = IMAPclient(IMAP_SERVER,IMAP_PORT)
    SMTP_client = SMTPclient()
    os.system('clear')
    print(Fore.LIGHTGREEN_EX + "\n\nPlease wait till connection is done"+Style.RESET_ALL)
    IMAP_client.START_CONN()
    IMAP_client.CAPABILITY()
    c = IMAP_client.LOGIN(username,password)
    os.system('clear')
    try:
        mails = IMAP_client.LIST()
    except:
        pass
    if 'gmail.com' in username:
            SMTP_SERVER =  'smtp.gmail.com'		
            SMTP_PORT = 587
    elif '@coep.ac.in' in username:
        SMTP_SERVER = 'smtp-mail.outlook.com'
        SMTP_PORT = 587
        # destination = 'bhandvicky2002@gmail.com'

    print(SMTP_PORT,"",SMTP_SERVER)
    SMTP_client.startSMTP(SMTP_SERVER,SMTP_PORT)
    c = SMTP_client.authenticate(username,password)
    os.system('clear')
    if c:
        break
    else:
        IMAP_client.CLOSE()
        SMTP_client.terminateSession()
        os.system('clear')
        print("Invalid Email ID")
        print("Enter Mail again")
while True:
    print(Fore.LIGHTBLUE_EX+ "\t\t\t EMAIL CLIENT"+Style.RESET_ALL)
    print("\n\n 1 . Compose an email")
    print(" 2 . Read emails")
    print(" 3 . Exit mail CLient")
    choice = int(input("Enter Choice here : "))
    if choice == 2:
        n = 0
        print(" 1. View all subject and date")
        print(" 2. View complete mails")
        select = int(input("Enter : "))
        mails = IMAP_client.LIST()
        if IMAP_SERVER == 'imap.gmail.com':
            
            for i,box in enumerate(mails):

                print(" ",i+1,".",box)
            mailbox = int(input("Enter mailbox number : "))
            n = IMAP_client.SELECT_MAILBOX(mails[mailbox-1])

        else:
            IMAP_client.SELECT_INBOX()
        if n == math.inf:
            print("No mails in the selected mailbox")
            break
        IMAP_client.SEARCH()
        if select == 2 :
            IMAP_client.FETCH()
        elif select == 1 :
            IMAP_client.FETCH_sub()
        else:
            print("Enter valid choice")
        # IMAP_client.FETCH_sub()
        # IMAP_client.CLOSE()
    if choice == 1:
        print('Enter EMAIL to WHICH you want to send the email')
        destination = input("Enter: ")
        
        
        msg = MIMEMultipart()
        # msg["From"] = input("From: ")
        # msg["To"] = input("To: ")
        msg["Subject"] = input("Subject: ")
        
        body = input("Type your mail: ")
        attachment = input("DO you want to insert attachment (Y/N) : ")
        filepath = ''
        if attachment.upper() == 'Y':
            filepath = input("Enter file path : ") 
        message = MIMEText(body, "plain")
        msg.attach(message)
        if filepath != '':
            attachment = open(filepath, "rb")

            part = MIMEBase('application', 'octet-stream')
            part.set_payload((attachment).read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', "attachment; filename= %s" % filepath)
            msg.attach(part)
        msgString = msg.as_string()
        SMTP_client.sendMail(username,destination,msgString)
        SMTP_client.terminateSession()

    if choice == 3:
        print("\nTHANK YOU")
        break
    else:
        print("Enter again")
    # smtp.ConnectMail()
# if __name__ == "__main__":
#     from imap import *
#     menu()
