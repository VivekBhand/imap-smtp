import base64
from socket import *
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText



from socket_conn import *
import sys
import base64

# SMTP client class to interact with SMTP server
class SMTPclient:
	def __init__(self):
		
		self.sock = SMTP()
		
	def startSMTP(self,mailserver,port):
		self.sock.connectMail(mailserver,port)
		self.sock.helo(mailserver)

	# Authenticate user on SMTP server	
	def authenticate(self,user,password):
		
		# # Base64 encodes credentials
		# userb64 = user.encode()
		# passb64 = password.encode()
		
		# Send request to login
		self.sock.sendCommand('AUTH LOGIN')
		
		# Server responds, requesting client to send username and password
		# self.sock.sendCommand(userb64)
		# authOutcome = self.sock.sendCommand(passb64)
		self.sock.log(user)
		authOutcome = self.sock.log(password)
		if authOutcome.split(' ')[0] == str(235):
			return True
		else:
			return False
		
	# Wrapper function to send email. Sends MAIL FROM, RCPT TO and DATA
	# commands in sequence as per SMTP protocol
	def sendMail(self,FROM,TO,body):
		self.sock.sendCommand('MAIL FROM:<' + FROM + '>')

		self.sock.sendCommand('RCPT TO:<' + TO + '>')
		self.sock.sendCommand('DATA')
		
		# Terminate message with CLRF a fullstop and another CLRF to indicate
		# to server end of message
		self.sock.sendCommand(body + '\n' + '\r\n' + '.' + '\r\n')
	
	
	def abortCurrentMailTransaction(self):
		self.sock.sendCommand('RSET')
		
	def doNothingJustGet250_OK(self):
		self.sock.sendCommand('NOOP')	
	
	def terminateSession(self):
		self.sock.sendCommand('QUIT')

	# Verify if a user exists on the server
	def verifyAddress(self,user):
		self.sock.sendCommand('VRFY ' + user)		
			
	
	def entercommand(self):
		while 1:
			msg = input('--> ')
			self.sock.sendCommand(msg)	
					


