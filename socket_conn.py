from os import path
from socket import *
import struct
import time
import ssl
import sys
import base64
from types import ClassMethodDescriptorType
from colorama import Fore,Style



class IMAP:
	def __init__(self):
		
		# Create socket
		self.socket = socket(AF_INET, SOCK_STREAM)

		self.socket = ssl.wrap_socket(self.socket,ssl_version=ssl.PROTOCOL_SSLv23)
		
		self.identifierNum = 0
		
	def connect(self,IMAP_SERVER,IMAP_PORT):
		self.socket.connect((IMAP_SERVER,IMAP_PORT))
		reply = self.getResponse()

	def sendMessage(self,cmd):
		alphNum = 'A00' + str(self.identifierNum)
		self.identifierNum+=1
		
		data = alphNum + ' ' + cmd + '\r\n'
		
		# Send client request in TCP stream
		reply = self.socket.send(data.encode())
		return reply

	def getSubject(self,cmd):
		
		
		alphNum = 'A00' + str(self.identifierNum)
		self.identifierNum+=1
		
		data = alphNum + ' ' + cmd + '\r\n'
		
		
		self.socket.send(data.encode())

		# CNF = alphNum + ' OK Success'
		reply = self.getResponse()
		# print(reply)
		n = 0
		while alphNum not in reply and 'OK' not in reply:
			
			if 'UID' in reply and 'FLAGS' in reply:
				reply = reply.split('}')
				FLAG = reply.pop(0)
				reply = ''.join(reply)
			if 'UID' in reply and 'FLAGS (\\' in reply :
				reply = reply.split("UID")
				FLAG = reply.pop(-1)
				reply = ''.join(reply)
			if '\Seen' in FLAG:
				print(Fore.GREEN + reply + Style.RESET_ALL)
			else:
				print(reply)
			reply = self.getResponse()

	def getMail(self,cmd):
		
		
		alphNum = 'A00' + str(self.identifierNum)
		self.identifierNum+=1
		
		data = alphNum + ' ' + cmd + '\r\n'
		
		
		self.socket.send(data.encode())

		# CNF = alphNum + ' OK Success'
		reply = self.getResponse()
		nreply = ''
		# print(reply)
		if alphNum in reply and 'BAD' in reply:
			print("Wrong command")
			return
		n = 0
		while alphNum not in reply and 'OK' not in reply:
			nreply = reply + nreply
			reply = self.getResponse()
		return nreply
			

	# Function used to send email to server and return reply from server	
	def sendCommand(self,cmd):

		alphNum = 'A00' + str(self.identifierNum)
		self.identifierNum+=1
		
		data = alphNum + ' ' + cmd + '\r\n'
		# Send client request in TCP stream
		self.socket.send(data.encode())
		# Receive server response from TCP stream
		reply = self.getResponse()
		if alphNum in reply and 'BAD' in reply:
			print("Wrong command")
			return
		while alphNum not in reply and 'OK' not in reply:
			reply = reply + self.getResponse()
		return reply
		
	def getResponse(self):
		replyToSentence = self.socket.recv(4096)
		return replyToSentence.decode()
		
		
		
class POP3:
	def __init__(self):
		self.socket = socket(AF_INET, SOCK_STREAM)
		if POP_PORT == 995:
			self.socket = ssl.wrap_socket(self.socket)
		self.socket.connect((POP_SERVER,POP_PORT))
		self.getResponse()
		self.identifierNum = 0
		
	def sendCommand(self,cmd):
		CLRF = '\r\n'
		data = cmd + CLRF
		self.socket.send(data)
		reply = self.getResponse()
		# print(reply)
		return reply
		
	def generateAlphaNumeric(self):
		alphNum = 'A00' + str(self.identifierNum)
		self.identifierNum+=1
		return alphNum
		
	def getResponse(self):
		replyToSentence = self.socket.recv(4096)
		return replyToSentence	



class SMTP:
	def __init__(self):	
		self.socket = socket(AF_INET, SOCK_STREAM)	
		
		
	def connectMail(self,mailserver,port):
		self.socket.connect((mailserver,port))
		recv = self.socket.recv(1024).decode()	
		print('Connected with Server')
		# print(recv)
		self.helo(mailserver)
		self.sendCommand("STARTTLS")
		

		if port == 465 or port ==587:
			# print(mailserver)
			if mailserver == 'smtp.gmail.com':
				self.socket = ssl.wrap_socket(self.socket,ssl_version=ssl.PROTOCOL_SSLv23)	
			elif mailserver == 'smtp-mail.outlook.com':
				self.socket = ssl.wrap_socket(self.socket,ssl_version=ssl.PROTOCOL_SSLv23)
		
		if recv[:3] != "220":
			print("250 reply not received from server.")

	def helo(self,mailserver):
		if mailserver == 'smtp-mail.outlook.com':
			helo_adr = "HELO " + 'outlook.com'
		else:
			helo_adr = "EHLO " + str(mailserver)
		# print(helo_adr)
		self.sendCommand(helo_adr)
		return

		
	def log(self,cmd):
		self.socket.send(base64.b64encode(cmd.encode()) + "\r\n".encode())
		# print(self.getResponse())
		return self.getResponse()

	# def send(self,cmd):
	# 	self.socket.send(cmd)
	# 	print(self.getResponse())


	def recv(self,cmd):
		self.socket.recv(cmd)
		return self.getResponse()
		
	def sendCommand(self,cmd):
		CLRF = '\r\n'
		data = cmd + CLRF
		self.socket.send(data.encode())
		return self.getResponse()		
		
	def getResponse(self):
		replyToSentence = self.socket.recv(4096)
		# print(replyToSentence.decode())
		return replyToSentence.decode()


