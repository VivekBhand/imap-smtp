from socket_conn import *
import math
import email
from email.parser import Parser
from email.mime.multipart import *
number = 0
class IMAPclient:
    def __init__(self,IMAP_SERVER,IMAP_PORT):
        self.IMAP_SERVER = IMAP_SERVER
        self.IMAP_PORT = IMAP_PORT
        self.sock = IMAP()
        self.numMsgs = 0
        self.UID = []

    def START_CONN(self):
        self.sock.connect(self.IMAP_SERVER,self.IMAP_PORT)

    def entercommand(self):
        msg = input("Enter : ")
        return self.sock.sendCommand(msg)
            
            
    def sendOwnText(self,msg):
        self.sock.sendCommand(msg)

    # Login command to server							
    def LOGIN(self, username, password):
        return self.sock.sendCommand('login ' + username + ' ' + password)

    def CAPABILITY(self):
        self.sock.sendCommand('CAPABILITY')


    def LOGOUT(self):
        self.sock.sendCommand('LOGOUT')

    def LIST(self):
        reply = self.sock.sendCommand('LIST "" "*"')
        mailboxes = []
        rstatus = reply.split("\r\n")
        rstatus.pop()
        rstatus.pop()
        for line in rstatus:
            inboxName = line.split('"')[-2]
            flags = line.split("(")
            flags = flags[1].split(")")
            flags = flags[0].split("\\")
            flags = flags[1:]
        #    print(flags)
            for i in range(len(flags)):
                flags[i] = flags[i].strip()
            #inboxName = inboxName[1:-1]
            if 'Noselect' not in flags:
                mailboxes.append(inboxName)
            # print(inboxName)
        return mailboxes

    def SELECT_MAILBOX(self,mail):
        mail = '"' + mail + '"'
        cmd = 'SELECT ' + mail
        reply = self.sock.sendCommand(cmd)
        self.numMsgs = int(reply.split(' EXISTS')[0].split('* ')[-1])
        if self.numMsgs == 0:
            return math.inf

    # Display messages in mailbox 'inbox'
    def SELECT_INBOX(self):
        reply = self.sock.sendCommand('SELECT INBOX')
        self.numMsgs = int(reply.split(' EXISTS')[0].split('* ')[-1])

    def SEARCH(self):
    
        reply = self.sock.sendCommand("UID SEARCH ALL")
        nreply = reply
        # reply = [int(i) for i in reply]
        while "OK" not in reply:
            reply = self.sock.getResponse()
            nreply = nreply + reply
            # replyArray.append('1')
        replyArray = nreply.split('SEARCH ')[1].split('\r\n')[0].split(' ')
        replyArray = [int(i) for i in replyArray]
        replyArray =  sorted(replyArray,reverse = True)
        replyArray = [str(i) for i in replyArray]
        self.UID = replyArray
        

    # List folders in mailbox (inbox in this case)
    def LIST_INBOX(self):
        reply = self.sock.sendCommand('LIST INBOX *')
        
    # Delete folder/inbox	
    def DELETE(self):
        self.sock.sendCommand('DELETE inbox')

    def FETCH_flag(self):
        pass

    def FETCH(self):
        # print(self.UID[:10])
        for i,UID in enumerate(self.UID[:10]):
            filename = 'mail' + UID +'.txt'   
            fp = open(filename,'a+')
            print('Message: ' + str(i+1))
            reply = self.sock.getMail('UID FETCH ' + UID + ' (BODY[1.MIME])')
            print(reply)
            reply = self.sock.getMail('UID FETCH ' + UID + ' (BODY[2.MIME])')
            print(reply)
            # reply = self.sock.getMail('UID FETCH ' + UID + ' (BODY[HEADER])')
            # fp.write(reply)	
            # # print(reply)
            # reply = self.sock.getMail('UID FETCH ' + UID + ' (BODY[TEXT])')
            # reply = self.sock.sendCommand('UID FETCH' + UID + ' (RFC822.HEADER BODYSTRUCTURE)')
            fp.write(reply)	
            
    # Fetch all emails on IMAP server and display to client headers and Date
    def FETCH_sub(self):
        # print(self.UID)
        global number

        if number + 10 < len(self.UID):
            upper = number + 10
        else:
            upper = len(self.UID)
        for i in range(number,upper):   
            print('Message: ' + str(i+1))
            self.sock.getSubject('UID FETCH ' + self.UID[i] + ' (FLAGS BODY[HEADER.FIELDS (DATE FROM)])')	
            # self.sock.getMail('UID FETCH ' + self.UID[i] + ' (BODY[HEADER.FIELDS (DATE FROM)])')			
        number = number + 10
    def LOGOUT(self):
        self.sock.sendCommand("LOGOUT")

    # subscribe/unsubscribe to mailing list
    def SUBSCRIBE(self,listname):
        self.sock.sendCommand('SUBSCRIBE ' + listname)
    def UNSUBSCRIBE(self,listname):
        self.sock.sendCommand('UNSUBSCRIBE ' + listname)	

    # Close session
    def CLOSE(self):
        self.sock.sendCommand('CLOSE')

    # Copies spcified emails to destination inbox (in this case a folder
    # called arbFolder	
    def COPY(self):
        self.sock.sendCommand('COPY 1:10 arbFolder')	
    #---------------------------------------------		

    # Create new inbox
    def CREATE(self):
        self.sock.sendCommand('CREATE /mailfolder')	


        ## STACK OVERFLOW

        # parser = Parser()

        #     email = parser.parsestr(reply)
        #     """
        #     print("email --")
        #     print(email)
        #     print("email --")
        #     """

        #     body = ""
        #     if email.is_multipart():
        #         for part in email.walk():
        #             if part.is_multipart():
        #                 for subpart in part.get_payload():
        #                     if subpart.is_multipart():
        #                         for subsubpart in subpart.get_payload():
        #                             body = body + str(subsubpart.get_payload(decode=True)) + '\n'
        #                         #    print("body: " + body)
        #                     else:
        #                         body = body + str(subpart.get_payload(decode=True)) + '\n'
        #                         #print("body: " + body)
        #             else:
        #                 body = body + str(part.get_payload(decode=True)) + '\n'
        #                 #print("body: " + body)
        #     else:
        #         body = body + str(email.get_payload(decode=True)) + '\n'
        #         #print("body: " + body)

        #     # body = body.decode()
        #     print(body)
        #     print(",,,,,,,,,,,,,,,,,,,,,,,,,,,")
        #     filehtml = filename + '.html'
        #     open(filehtml, "w").close() 
        #     f=open(filehtml,"w+")
        #     whole_msg = body
        #     text_msg = whole_msg.split('html>')
        #     for i in range(len(text_msg)):
        #         #if i % 2 == 0:
        #             # = '</br'.join(text_msg.split('\n')
        #         text1_msg = text_msg[i].split('<html')
        #         text1_msg[0] = text1_msg[0].replace('\n', '</br>')
        #         # text1_msg = [i.decode() for i in text1_msg]
        #         text_msg[i] = ''.join(text1_msg)
        #             #text_msg[i] = text_msg[i].replace('\n', '</br>')
        #     #f.write('</br>'.join(body.encode('utf-8').split('\n')))
        #     #print(body.encode('utf-8'))
        #     #f.write(body.encode('utf-8'))
            
        #     f.write(''.join(text_msg))
        #     #print('written')
        #     f.close()
    	
        


