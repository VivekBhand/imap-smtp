# imap-smtp

Developing Email client as a project for Computer Networks course

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:7df46f8062fd3adfe1fc94e63698a62c?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:7df46f8062fd3adfe1fc94e63698a62c?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:7df46f8062fd3adfe1fc94e63698a62c?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/VivekBhand/imap-smtp.git
git branch -M main
git push -uf origin main
```
